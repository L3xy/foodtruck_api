'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _config = require('./config');

var _config2 = _interopRequireDefault(_config);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var LocalStrategy = require('passport-local').Strategy;

//Create the app
var app = (0, _express2.default)();
app.server = _http2.default.createServer(app);

//middleware
//parse application/json
app.use(_bodyParser2.default.json({
  limit: _config2.default.bodyLimit
}));

//passport config
app.use(_passport2.default.initialize()); //initialize passport
var Account = require('./model/account');
_passport2.default.use(new LocalStrategy({
  usernameField: 'email', //map usernameField to email field in the model
  passwordField: 'password' //mapp passwordField to password field in the model
}, Account.authenticate()));
_passport2.default.serializeUser(Account.serializeUser());
_passport2.default.deserializeUser(Account.deserializeUser());

//api routes v1
app.use('/api/v1', _routes2.default); //direct all calls to.. v1 to routes

app.server.listen(_config2.default.port); //setting the port to whats in config file
console.log('Server started on port ' + app.server.address().port);

exports.default = app;
//# sourceMappingURL=index.js.map