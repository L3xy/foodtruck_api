'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _express = require('express');

var _foodtruck = require('../model/foodtruck');

var _foodtruck2 = _interopRequireDefault(_foodtruck);

var _review = require('../model/review');

var _review2 = _interopRequireDefault(_review);

var _authMiddleware = require('../middleware/authMiddleware');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//create FoodTruck class from the model
exports.default = function (_ref) {
  var config = _ref.config,
      db = _ref.db;

  var api = (0, _express.Router)();

  // CRUD - Create Read Update Delete


  // 'v1/foodtruck/add'
  api.post('/add', _authMiddleware.authenticate, function (req, res) {
    // use the authenticate to lock it down by requiring the token
    var newFoodTruck = new _foodtruck2.default(); //createServer a new reataurant object from the 'Retaurant' class
    newFoodTruck.name = req.body.name;
    newFoodTruck.foodtype = req.body.foodtype;
    newFoodTruck.avgcost = req.body.avgcost;
    newFoodTruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
    newFoodTruck.geometry.coordinates.long = req.body.geometry.coordinates.long;

    newFoodTruck.save(function (err) {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'FoodTruck saved successfully' });
    });
  });

  // 'v1/foodtruck' - Read
  api.get('/', function (req, res) {
    _foodtruck2.default.find({}, function (err, foodtrucks) {
      if (err) {
        res.send(err);
      }
      res.json(foodtrucks); //return all foodtruck in json format
    });
  });

  // '/v1/foodtruck/:id' = Read 1
  api.get('/:id', function (req, res) {
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      res.json(foodtruck);
    });
  });

  // '/v1/foodtruck/:id' - Update
  api.put('/:id', _authMiddleware.authenticate, function (req, res) {
    // use the authenticate to lock it down by requiring the token
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      foodtruck.name = req.body.name;
      foodtruck.foodtype = req.body.foodtype;
      foodtruck.avgcost = req.body.avgcost;
      foodtruck.geometry.coordinates.lat = req.body.geometry.coordinates.lat;
      foodtruck.geometry.coordinates.long = req.body.geometry.coordinates.long;

      foodtruck.save(function (err) {
        if (err) {
          res.send(err);
        }
        res.json({ message: "FoodTruck info updated" });
      });
    });
  });

  //'/v1/foodtruck/:id' - Delete
  api.delete('/:id', _authMiddleware.authenticate, function (req, res) {
    // use the authenticate to lock it down by requiring the token
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.status(500).send(err); //Return error 500 (internal server error) if we find error
        return; //if error is found, execution is halt
      }
      if (foodtruck === null) {
        res.status(404).send("FoodTruck not found"); //If foodtruck is null return error 400 (not found) 
        return;
      }
      _foodtruck2.default.remove({
        _id: req.params.id
      }, function (err, foodtruck) {
        if (err) {
          res.status(500).send(err);
          return; //if error is found, execution is halt
        }
        _review2.default.remove({
          foodtruck: req.params.id
        }, function (err, review) {
          if (err) {
            res.send(err);
          }
          res.json({ message: "FoodTruck and Associated Reviews successfully Removed" });
        });
      });
    });
  });

  //add review for specific foodtruck id
  // '/v1/foodtruck/review/add/:id'
  api.post('/reviews/add/:id', _authMiddleware.authenticate, function (req, res) {
    // use the authenticate to lock it down by requiring the token
    _foodtruck2.default.findById(req.params.id, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      var newReview = new _review2.default();

      newReview.title = req.body.title;
      newReview.text = req.body.text;
      newReview.foodtruck = foodtruck._id;
      newReview.save(function (err, review) {
        if (err) {
          res.send(err);
        }
        foodtruck.reviews.push(newReview); //save into the array
        foodtruck.save(function (err) {
          if (err) {
            res.send(err);
          }
          res.json({ message: 'Food truck review saved!' });
        });
      });
    });
  });

  //get reviews for a specific food truck id
  // 'v1/foodtruck/reviews/:id'
  api.get('/reviews/:id', function (req, res) {
    _review2.default.find({ foodtruck: req.params.id }, function (err, reviews) {
      if (err) {
        res.send(err);
      }
      res.json(reviews);
    });
  });

  // get food truck for specific foodtypes
  // '/v1/foodtruck/foodtype/:foodtype'
  api.get('/foodtype/:foodtype', function (req, res) {
    _foodtruck2.default.find({ foodtype: req.params.foodtype }, function (err, foodtruck) {
      if (err) {
        res.send(err);
      }
      res.json(foodtruck);
    });
  });

  return api;
};
//# sourceMappingURL=foodtruck.js.map