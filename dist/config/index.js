"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  "port": process.env.PORT || 3005,
  "mongoUrl": process.env.MONGOURL || "mongodb://localhost:27017/foodtruck-api",
  "bodyLimit": process.env.BODYLIMIT || "100kb"
};
//# sourceMappingURL=index.js.map